package java8StreamsPractising.transactionsExercise;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TransactionsMain {
    public static void main(String[] args) {
        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300),
                new Transaction(raoul, 2012, 1000),
                new Transaction(raoul, 2011, 400),
                new Transaction(mario, 2012, 710),
                new Transaction(mario, 2012, 700),
                new Transaction(alan, 2012, 950)
        );

        List<Transaction> transactions2011 = transactionsLessThan2011(transactions);
        List<String> distinctCity = distinctCity(transactions);
        List<Trader> cambridgeTraders = cambridgeTraders(transactions);
        String tradersNames = tradersNames(transactions);

        boolean milan = transactions.stream()
                .anyMatch(transaction -> transaction.getTrader().getCity().equals("Milan"));

        tradersFromCambridge(transactions);
        Optional<Integer> highestValue = highestValue(transactions);
        highestValue.ifPresent(o -> System.out.println("highest value: " + o));

        Optional<Transaction> max = transactions.stream()
                .max(Comparator.comparing(Transaction::getValue));
        max.ifPresent(o -> System.out.println("max: " + o));
    }

    private static Optional<Integer> highestValue(List<Transaction> transactions) {
        return transactions.stream()
                .map(Transaction::getValue)
                .reduce(Integer::max);
    }

    private static void tradersFromCambridge(List<Transaction> transactions) {
        transactions.stream()
                .filter(t -> t.getTrader().getCity().equals("Cambridge"))
                .map(Transaction::getValue)
                .forEach(System.out::println);
    }

    private static String tradersNames(List<Transaction> transactions) {
        return transactions.stream()
                .map(t -> t.getTrader().getName())
                .distinct()
                .sorted()
                .collect(Collectors.joining());
    }

    private static List<Trader> cambridgeTraders(List<Transaction> transactions) {
        return transactions.stream()
                .map(Transaction::getTrader)
                .filter(t -> t.getCity().equals("Cambridge"))
                .distinct()
                .sorted(Comparator.comparing(Trader::getName))
                .collect(Collectors.toList());
    }

    private static List<String> distinctCity(List<Transaction> transactions) {
        return transactions.stream().map(t -> t.getTrader().getCity()).distinct().collect(Collectors.toList());
    }

    private static List<Transaction> transactionsLessThan2011(List<Transaction> transactions) {
        return transactions.stream()
                .filter(t -> t.getYear() < 2011)
                .sorted(Comparator.comparing(Transaction::getValue))
                .collect(Collectors.toList());
    }
}
