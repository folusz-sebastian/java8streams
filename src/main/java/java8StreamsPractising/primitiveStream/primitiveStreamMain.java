package java8StreamsPractising.primitiveStream;

import java.util.stream.IntStream;

public class primitiveStreamMain {
    public static void main(String[] args) {
        IntStream intStream = IntStream.rangeClosed(1, 100)
                .filter(n -> n % 2 == 0);

        System.out.println(intStream.count());
    }
}
