package java8StreamsPractising;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StreamFromArray {
    public static void main(String[] args) {
        List<String> lines = new ArrayList<>(3);
        lines.add("nknfsd nfjnd jfndsjnfsdkfnds fndsf e");
        lines.add("kjncnn fjen nf nfdls");
        lines.add("jdsooe kmfdkmkmd kkkk");

        long count = lines.stream().flatMap(line -> Arrays.stream(line.split(" "))).count();
        System.out.println(count);
    }
}
